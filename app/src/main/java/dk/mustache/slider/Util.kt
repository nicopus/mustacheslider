package dk.mustache.slider

import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.graphics.drawable.shapes.Shape
import android.graphics.drawable.GradientDrawable



fun Float.pxToDp(): Float {
    return (this / Resources.getSystem().displayMetrics.density)
}

fun Float.dpToPx(): Float {
    return (this * Resources.getSystem().displayMetrics.density)
}

fun Int.pxToDp(): Int {
    return (this / Resources.getSystem().displayMetrics.density).toInt()
}
fun Int.dpToPx(): Int {
    return (this * Resources.getSystem().displayMetrics.density).toInt()
}

fun Float.pxToSp(): Float {
    return (this / Resources.getSystem().displayMetrics.scaledDensity)
}

fun getSquareShape(cornerRadius: Float, color: Int): Drawable {
    val shape = GradientDrawable()
    shape.shape = GradientDrawable.RECTANGLE
    shape.cornerRadius = cornerRadius
    shape.setColor(color)
    return shape
}

fun getCircleShape(color: Int): Drawable {
    val shape = GradientDrawable()
    shape.shape = GradientDrawable.OVAL
    shape.setColor(color)
    return shape
}

