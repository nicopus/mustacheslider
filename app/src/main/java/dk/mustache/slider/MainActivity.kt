package dk.mustache.slider

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val selectedTextView = findViewById<TextView>(R.id.selectedTextView)
        val slider = findViewById<MustacheSlider>(R.id.slider)
        slider.addOnNumberSelectedListener(object: MustacheSlider.OnNumberSelectedListener {
            override fun onSelected(number: Int) {
                val selectedText = getString(R.string.selected_number, number.toString())
                selectedTextView.text = selectedText
            }
        })


    }
}