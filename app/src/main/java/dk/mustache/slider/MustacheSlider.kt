package dk.mustache.slider

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Px
import androidx.core.animation.doOnEnd
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.children
import androidx.core.view.setPadding
import androidx.core.view.updateLayoutParams
import kotlin.math.abs

class MustacheSlider(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    interface OnNumberSelectedListener {
        fun onSelected(number: Int)
    }

    private val listeners = mutableListOf<OnNumberSelectedListener>()

    companion object {
        const val DEFAULT_ANIMATION_DURATION = 200L
        const val SHORT_ANIMATION_DURATION = 150L
    }

    private var selectedIndex = 0
    private val selectedValue: Int
        get() = minValue + selectedIndex
    private val minValue: Int
    private val maxValue: Int
    private val numberOfTicks: Int
        get() = maxValue - minValue + 1
    private val numberTextSize: Float
    private val thumpBackgroundColor: Int
    private val activeNumberTextColor: Int
    private val inactiveNumberBackgroundColor: Int
    private val inactiveNumberTextColor: Int
    @Px private val thumpPadding: Int

    // View variables
    private val inactiveNumbersContainer: LinearLayout
    private val inactiveNumberViews = mutableListOf<TextView>()
    private val thumpContainer: FrameLayout
    private val thumpView: View
    private val indicatorView: TextView
    private val activeNumbersContainer: LinearLayout
    private val activeNumberViews = mutableListOf<TextView>()

    // Size and position variables
    private var viewHeight = 0
    private var viewWidth = 0
    private var viewX = 0F
    private var viewY = 0F
    private val sliderY: Float
        get() = ((viewY + (viewHeight.toFloat() / 2)) - (thumpWidth.toFloat() / 2))
    private val thumpWidth: Int
        get() = viewWidth / numberOfTicks
    private val thumpHeight: Int
        get() = thumpWidth
    private var thumpDeltaX = 0F
    private var minThumpX = 0F
    private var maxThumpX = 0F
    private val cornerRadius: Int
        get() = thumpWidth / 2
    private val indicatorBounce: Int
        get() = thumpHeight / 4

    init {
        val sa = context.obtainStyledAttributes(attrs, R.styleable.MustacheSlider)
        minValue = sa.getInt(R.styleable.MustacheSlider_minValue, 0)
        maxValue = sa.getInt(R.styleable.MustacheSlider_maxValue, 10)
        numberTextSize = sa.getDimension(R.styleable.MustacheSlider_textSize, 18F)
        thumpBackgroundColor = sa.getColor(R.styleable.MustacheSlider_thumpColor, Color.WHITE)
        activeNumberTextColor = sa.getColor(R.styleable.MustacheSlider_thumpTextColor, Color.BLACK)
        inactiveNumberBackgroundColor = sa.getColor(R.styleable.MustacheSlider_tickColor, Color.BLACK)
        inactiveNumberTextColor = sa.getColor(R.styleable.MustacheSlider_tickTextColor, Color.WHITE)
        thumpPadding = sa.getDimension(R.styleable.MustacheSlider_thumpPadding, 0F).toInt()


        sa.recycle()

        indicatorView = createNumberIndicatorView()
        addView(indicatorView)

        inactiveNumbersContainer = createInactiveNumbersContainer()
        for (value in minValue..maxValue) {
            val tickView = createInactiveNumberView(value)
            inactiveNumberViews.add(tickView)
            inactiveNumbersContainer.addView(tickView)
        }
        addView(inactiveNumbersContainer)

        thumpContainer = buildThumpContainer()
        thumpView = buildThumpView()
        thumpContainer.addView(thumpView)
        addView(thumpContainer)

        // Active numbers in front of thump view. When thump is released, the closes active number
        // view is shown, and all other number views are hidden.
        activeNumbersContainer = createNumbersContainer()
        for (value in minValue..maxValue) {
            val selectedNumberView = createActiveNumberView(value)
            activeNumberViews.add(selectedNumberView)
            activeNumbersContainer.addView(selectedNumberView)
        }
        addView(activeNumbersContainer)

        hideAllActiveNumbers()
        showSelectedActiveNumber()

        this.addOnLayoutChangeListener { _, left, top, right, bottom, _, _, _, _ ->
            val height = bottom - top
            val width = right - left
            // If view height or width have change, resize and reposition compound views
            if (height != viewHeight || width != viewWidth ) {
                viewHeight = height
                viewWidth = width
                viewX = left.toFloat()
                viewY = top.toFloat()
                resizeAndReposition()
            }
        }
    }

    fun addOnNumberSelectedListener(listener: OnNumberSelectedListener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener)
        }
    }

    fun removeOnNumberSelectedListener(listener: OnNumberSelectedListener) {
        listeners.remove(listener)
    }

    private fun resizeAndReposition() {
        inactiveNumbersContainer.updateLayoutParams {
            width = viewWidth
            height = thumpHeight
        }
        inactiveNumbersContainer.y = sliderY

        inactiveNumbersContainer.children.forEach {
            it.updateLayoutParams {
                width = thumpWidth
                height = thumpHeight
            }
        }
        activeNumbersContainer.updateLayoutParams {
            width = viewWidth
            height = thumpHeight
        }
        activeNumbersContainer.y = sliderY
        activeNumbersContainer.children.forEach {
            it.updateLayoutParams {
                width = thumpWidth
                height = thumpHeight
            }
        }
        thumpContainer.updateLayoutParams {
            width = viewWidth
            height = thumpHeight
        }
        thumpContainer.y = sliderY
        thumpView.updateLayoutParams {
            width = thumpWidth
            height = thumpHeight
        }
        indicatorView.updateLayoutParams {
            width = thumpWidth
            height = thumpHeight
        }
        indicatorView.y = sliderY

        maxThumpX = (viewWidth - thumpWidth).toFloat()

        inactiveNumbersContainer.background = getSquareShape(cornerRadius.dpToPx().toFloat(), inactiveNumberBackgroundColor)
        indicatorView.background = getSquareShape(cornerRadius.dpToPx().toFloat(), inactiveNumberBackgroundColor)
    }

    private fun createNumberView(value: Int): TextView {
        return TextView(context).apply {
            this.layoutParams = LinearLayout.LayoutParams(
                thumpWidth,
                thumpHeight
            )
            this.gravity = Gravity.CENTER
            this.text = value.toString()
            this.textSize = numberTextSize.pxToSp()
        }
    }

    private fun createNumbersContainer(): LinearLayout {
        return LinearLayout(context).apply {
            this.orientation = LinearLayout.HORIZONTAL
            this.layoutParams = LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                0
            )
        }
    }

    private fun createInactiveNumbersContainer(): LinearLayout {
        return createNumbersContainer().apply {
            this.background = getSquareShape(cornerRadius.dpToPx().toFloat(), inactiveNumberBackgroundColor)
        }
    }

    private fun createInactiveNumberView(value: Int): TextView {
        return createNumberView(value).apply {
            this.typeface = ResourcesCompat.getFont(context, R.font.open_sans_regular)
            this.setTextColor(inactiveNumberTextColor)
        }
    }

    private fun createActiveNumberView(value: Int): TextView {
        return createNumberView(value).apply {
            this.typeface = ResourcesCompat.getFont(context, R.font.open_sans_bold)
            this.setTextColor(activeNumberTextColor)
        }
    }

    private fun buildThumpContainer(): FrameLayout {
        return FrameLayout(context).apply {
            this.layoutParams = LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                thumpHeight
            )
        }
    }

    private fun createNumberIndicatorView(): TextView {
        return createNumberView(selectedValue).apply {
            this.typeface = ResourcesCompat.getFont(context, R.font.open_sans_bold)
            this.setTextColor(activeNumberTextColor)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun buildThumpView(): FrameLayout {
        val thumpView = FrameLayout(context).apply {
            this.layoutParams = LayoutParams(
                thumpWidth,
                thumpHeight
            )
            this.setPadding(thumpPadding)
        }

        val shapeView = View(context).apply {
            this.layoutParams = LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            this.background = getCircleShape(thumpBackgroundColor)
        }

        thumpView.addView(shapeView)

        thumpView.setOnTouchListener { view, motionEvent ->
            handleOnThumpTouch(view, motionEvent)
            true
        }

        return thumpView
    }

    private fun handleOnThumpTouch(view: View, motionEvent: MotionEvent) {
        when (motionEvent.action) {

            MotionEvent.ACTION_DOWN -> {
                performClick()
                thumpDeltaX = motionEvent.rawX - view.x

                hideAllActiveNumbers()
                animateIndicatorY(
                    y1 = indicatorView.y,
                    y2 = sliderY - thumpHeight - indicatorBounce,
                    y3 = sliderY - thumpHeight
                )
            }

            MotionEvent.ACTION_MOVE -> {
                val (index, _) = findClosestTick(view.x)
                indicatorView.text = (minValue + index).toString()

                var newX = motionEvent.rawX - thumpDeltaX
                if (newX < minThumpX) {
                    newX = minThumpX
                } else if (newX > maxThumpX) {
                    newX = maxThumpX
                }
                view.x = newX
                indicatorView.x = newX
            }

            MotionEvent.ACTION_UP -> {
                val (index, newX) = findClosestTick(view.x)
                selectedIndex = index

                showSelectedActiveNumber()
                animateThumpAndIndicator(view.x, newX)

                animateIndicatorY(
                    startY = indicatorView.y,
                    endY = sliderY,
                    doOnEnd = {
                        listeners.forEach { it.onSelected(selectedValue) }
                    }
                )
            }
        }
    }

    private fun hideAllActiveNumbers() {
        activeNumberViews.forEach {
            it.visibility = View.INVISIBLE
        }
    }

    private fun showSelectedActiveNumber() {
        activeNumberViews[selectedIndex].visibility = View.VISIBLE
    }

    private fun findClosestTick(x: Float): Pair<Int, Float> {
        var index = 0
        var minDeltaX = Float.MAX_VALUE
        var minX = 0F
        for (i in 0 until inactiveNumberViews.size) {
            val delta = abs(inactiveNumberViews[i].x - x)
            if (delta < minDeltaX) {
                minDeltaX = delta
                minX = inactiveNumberViews[i].x
                index = i
            }
        }
        return Pair(index, minX)
    }

    private fun animateThumpAndIndicator(startX: Float, endX: Float) {
        ValueAnimator.ofFloat(startX, endX).apply {
            addUpdateListener { updatedAnimation ->
                thumpView.x = updatedAnimation.animatedValue as Float
                indicatorView.x = updatedAnimation.animatedValue as Float
            }
            duration = DEFAULT_ANIMATION_DURATION
            start()
        }
    }

    private fun animateIndicatorY(startY: Float, endY: Float, doOnEnd: (() -> Unit)? = null) {
        val animator = ValueAnimator.ofFloat(startY, endY).apply {
            addUpdateListener { updatedAnimation ->
                indicatorView.y = updatedAnimation.animatedValue as Float
            }
            duration = DEFAULT_ANIMATION_DURATION
        }
        doOnEnd?.let {
            animator.doOnEnd { doOnEnd.invoke() }
        }
        animator.start()
    }

    private fun animateIndicatorY(y1: Float, y2: Float, y3: Float) {
        val animator1 = ValueAnimator.ofFloat(y1, y2).apply {
            addUpdateListener { updatedAnimation ->
                indicatorView.y = updatedAnimation.animatedValue as Float
            }
            duration = DEFAULT_ANIMATION_DURATION
        }

        val animator2 = ValueAnimator.ofFloat(y2, y3).apply {
            addUpdateListener { updatedAnimation ->
                indicatorView.y = updatedAnimation.animatedValue as Float
            }
            duration = SHORT_ANIMATION_DURATION
        }

        animator1.doOnEnd {
            animator2.start()
        }
        animator1.start()
    }
}